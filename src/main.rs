use bevy::prelude::*;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins.set(ImagePlugin::default_nearest())) // prevents blurry sprites
        .insert_resource(Msaa { samples: 4 })
        .add_startup_system(setup)
        .add_system(movement)
        .add_system(follow_player)
        .run()
}

#[derive(Component)]
struct Player;

#[derive(Component)]
struct PlayerCam {
    follow_speed: f32,
}

#[derive(Component)]
struct Movement {
    speed: f32,
}

fn setup(
    mut commands: Commands,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
    asset_server: Res<AssetServer>,
) {
    let texture_handle = asset_server.load("gabe-idle-run.png");
    let texture_atlas =
        TextureAtlas::from_grid(texture_handle, Vec2::new(24.0, 24.0), 7, 1, None, None);
    let texture_atlas_handle = texture_atlases.add(texture_atlas);
    commands.spawn(Camera2dBundle::default());
    /*
    commands.spawn(SpriteBundle {
        texture: asset_server.load("walk_on_ferris.png"),
            transform: Transform::from_scale(Vec3::splat(1.)),
        ..default()
    });
    */
    commands.spawn((
        SpriteSheetBundle {
            texture_atlas: texture_atlas_handle,
            transform: Transform::from_scale(Vec3::splat(6.0)),
            ..default()
        },
        Movement { speed: 1.7 },
        Player,
    ));
}

fn movement(
    //mut commands: Commands,
    input: Res<Input<KeyCode>>,
    time: Res<Time>,
    mut query: Query<(&mut Transform, &Movement), With<Player>>,
) {
    for (mut transform, movement) in query.iter_mut() {
        if input.pressed(KeyCode::Up) {
            transform.translation.y += time.delta_seconds() * 150. * movement.speed;
        }
        if input.pressed(KeyCode::Down) {
            transform.translation.y -= time.delta_seconds() * 150. * movement.speed;
        }
        if input.pressed(KeyCode::Right) {
            transform.translation.x += time.delta_seconds() * 150. * movement.speed;
        }
        if input.pressed(KeyCode::Left) {
            transform.translation.x -= time.delta_seconds() * 150. * movement.speed;
        }
    }
}

fn follow_player(
    mut camera_query: Query<(&mut Transform, &PlayerCam), Without<Player>>,
    mut player_query: Query<&Transform, With<Player>>,
) {
    for (mut p_cam_t, _) in camera_query.iter_mut() {
        for p_t in player_query.iter_mut() {
            if p_cam_t.translation.y != p_t.translation.y {
                p_cam_t.translation.y = p_t.translation.y
            }
            if p_cam_t.translation.x != p_t.translation.x {
                p_cam_t.translation.x = p_t.translation.x
            }
        }
    }
}
